### C# Tutorial

## 初始步骤

#### step 1 创建项目

我们常用的项目类型如下，其中第3种是描述性写法，针对多平台的，我们一般不使用，兼容性还不是很好。

+ 控制台应用程序
+ WinForm 应用程序
+ WPF 应用程序

控制台程序考虑到的主要是语法层面的，相关的技术涵盖在winForm应用中了，就不单独介绍。

####  step 2 基本配置

1. 目录调整，如下图所示

      ![1538287136699](.\images\1538287136699.png)

      创建ico 和UI目录，移动form1.cs 和相对应的设计器跟踪生产的同前缀文件到UI，在项目中移除之前的文件再次创建UI组并添加现有文件，编译成功。

2. 工程属性配置

    在工程名上右键选择属性，这里可以配置工程的基本属性配置，我们主要看3个项目

   - name space , 修改成sluan，如果是自己的项目可以用自己的
   - SDK 版本，推荐用4.5，如果要兼容XP，只能用3.5
   -  icon 图标，可以到这个网站下载 https://www.easyicon.net/

   不是同name space 默认是不能直接引用的，考虑之前生成的代码中的name space 不同，需要将相关的cs文件中的name space 都修改工程设置中相同，此外可以顺便将UI目录下的文件名修改。文件名修改建议通过VS中右键进行，其可推导其他修改处。

## 基本使用

#### UI 控件的添加以及设计器的使用

VS的控件设计很友好，直接可以拖拽，双击可以添加默认的消息处理函数。在操作之前需要认识和控件想关的一些概念。 

**所属线程**  设计器生成的代码和事件执行所在的线程是受系统管理的UI线程，所以不会设计到跨线程操作。如果是自己添加到代码，用到了跨线程的部分或是在其他线程中调用了窗口控件，调试或编译时会提示跨线程操作。解决版本后续章节详细给出。

**响应时间**  为了用户友好，用户点击按钮后要及时的响应，不能长时间的占用所属线程的时间。

**属性**  如下图：

![1538288679850](.\images\1538288679850.png)

每一个控件都有属性页，主窗体也有。分为属性和事件，在设计器中的修改会同步到代码中，所以如果要做动态修改属性就不要问怎么实现了，可以参考设计器生成的代码。
*常规写法* 

+ 关闭主窗体的最大化
+ 禁止拉动窗体的大小
+ 调整字体 
+ 控件名称手动修改，否则是按数字向上递增

**控件更新时间**   控件可以手动强制刷新或者等控件操作完成之后系统调用自动刷新，其中窗体移动肯定也会发生重绘的。在研究绘图功能时会对窗体绘制特别研究。

#### 特殊控件介绍

​         控件分为可显示和不显示的，不显示的控件的主要作用是提供一种设计器的操作方法，和继承自control类的还是有些区别的。

-  进度条

   进度条拿出来的原因是我们不能像C语言中写代码一样调用修改进度值然后用delay 之类的操作，因为窗体没有响应时间。

- 定时器

  定时器在系统中有很多，在窗体中建议用窗体的定时器，在其他线程中理论上讲是不能用这个定时器的。这个定时器的回调是在主线程中运行的，不涉及到线程同步问题。

  ```
  this.timer1 = new System.Windows.Forms.Timer(this.components);
  ```

- 串口

  串口的控件的作用其实不大，只是提供了设置初始值参数的作用，具体的方法还得看代码操作。如果不知到有哪些事件可以绑定可以查看它的事件列表。

  ![1538292777862](.\images\1538292777862.png)



   如上图，可以看到可以绑定3个事件，和中断有些类似的概念。



## 设计一个简单的串口调试工具

  想好具体的功能

-   日志
-    换行显示和带时间戳显示
-    串口的模式和参数
-    16进制和其他形式（ASCII，GB2312，UTF-8)
-    定时发送

####　step1 绘制主窗口

在新建的工程上绘制主窗体，如下图：

![1540107687234](.\images\1540107687234.png)

特殊点：

​	1、主窗体的LOGO和应用的LOGO是分开设置的。

​	2、主窗体的标题可以通过属性直接配置也可以在程序中修改。

​        3、窗体的字体会影响窗体的大小。

​        4、建议关关闭 max 功能（MaximizeBox=false)和选择固定窗体大小（FormBorderStyle=FixedSingle)

​        5、控件通过Group分区，方便拖动和禁止操作。

​	6、下拉框默认是可以输入的，需要修改成只读（DropDownStyle=DropDownList）

#### step2 添加串口扫描和设置

我这里为了方便，把串口的操作设计在一个独立的控件里，和直接设计没多大的区别。串口主要关系3个动作，软件打开（窗体加载事件，双击主窗体），按钮”打开“，按钮“刷新”。代码如下：

```c#
private void CSerialPanel_Load(object sender, EventArgs e)
{
    //load the port list
    btnFresh_Click(null, null);
    // baudrate,default 115200
    cmbBaud.SelectedIndex = 6;

private void btnFresh_Click(object sender, EventArgs e)
{
    cmbPorts.Items.Clear();
    string[] portList = SerialPort.GetPortNames();
    if (portList.Length > 0)
    {
        cmbPorts.Items.AddRange(portList);
        cmbPorts.SelectedIndex = 0;
        btnOpen.Enabled = true;
    }
    else
    {
        btnOpen.Enabled = false;
    }

private void CtrLock(bool enable)
{
    cmbBaud.Enabled = !enable;
    cmbPorts.Enabled = !enable;
    btnFresh.Enabled = !enable;

private void btnOpen_Click(object sender, EventArgs e)
{
    Button btn = sender as Button;
    if (btn.Text == "Open")
    {
        //open the port
        string portname = this.cmbPorts.SelectedItem.ToString();
        Console.WriteLine("Port name:" + portnam
        try
        {
            _port = new SerialPort(portname);
            _port.BaudRate = Convert.ToInt32(this.cmbBaud.SelectedItem.ToString());
            _port.Open();         
            if (PortOpened != null)
            {
                PortOpened(sender, e);
            }
            btn.Text = "Close";
            CtrLock(true);
        }
        catch (Exception ex)
        {
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            _port.Close();
            _port = null;
      
    }
    else
    {
        //close the port ,always ok
        try
        {
            _port.Close();
        }
        finally
        {
            btn.Text = "Open";
            CtrLock(false);
            _port = null;
            if (PortClosed != null)
            {
                PortClosed(sender, e);
            }
        }
    }
}
```
代码比较简单，就是刷新串口列表，打开和关闭串口，然后提供了一个函数代理，用来和其他代码交互。

在主窗体的load 事件中关联串口的打开和关闭，控制Send 按钮是否可用。

```c#
private void PortOpened_Event(object sender,EventArgs e)
{
    btnSend.Enabled = true;

private void PortClosed_Event(object sender, EventArgs e)
{
    btnSend.Enabled = false;

private void MainForm_Load(object sender, EventArgs e)
{
    btnSend.Enabled = fal
    SerialPanel.PortClosed = PortClosed_Event;
    SerialPanel.PortOpened = PortOpened_Event;
    //.........
}
```
#### step3 处理发送函数

这部分简单的实现方式就是读取输入框中的内容，然后发送到串口。设及的内容是输入框中如果是16进制如何处理。

查找第三方包的的网站： https://www.nuget.org/

注意事项：

 	1、Textbox的控件属性中要选中支持多行。

​	2、这里使用TextBox不使用richTextBox的原因是功能能够满足，而且TextBox的换行有“\r\n",richTextBox只有”\n"。

支持字符部分的代码如下：

```c#
string str = rtbSend.Text;
if (string.IsNullOrEmpty(str))
{
    //nothing to do
    return;
}
if (true)
{
    //just put out
    Console.WriteLine("Send string: " + str);
    try
    {
        SerialPanel.Port.Write(str);
    }
    catch(Exception ex)
    {
        MessageBox.Show(ex.Message, "Error",  MessageBoxButtons.OK, MessageBoxIcon.Error);
        //post msg to the button 
        SerialPanel.Exception_close();
    }
}
```
接下来使用单选框，支持HEX发送。

双击单选框添加事件处理函数，进行简单的16进制和字符转换，如下：

```c#
private void cbHexSend_CheckedChanged(object sender, EventArgs e)
{
    if (string.IsNullOrEmpty(rtbSend.Text))
    {
        //nothing to do
        return;
    }
    //
    CheckBox cbbtn = sender as CheckBox;
    if (cbbtn.Checked)
    {
        //string to hex
        byte[] bvalue = System.Text.ASCIIEncoding.ASCII.GetBytes(rtbSend.Text);
        string str = HexConvert.ToHexString(bvalue);
        rtbSend.Text = str;
    }
    else
    {
        //hex to string
        byte[] bvalue = HexConvert.ToBytes(rtbSend.Text);
        string str = System.Text.ASCIIEncoding.ASCII.GetString(bvalue);
        rtbSend.Text = str;
    }
}
```
添加HEX支持的函数如下：
```c#
private void btnSend_Click(object sender, EventArgs e)
  {
      //do stuff with send
      //do with  string
      string str = rtbSend.Text;
      if (string.IsNullOrEmpty(str))
      {
          //nothing to do
          return;
      }
     if (cbHexSend.Checked)
        {
            try
            {
               byte[] bvalue = HexConvert.ToBytes(str);
               if (bvalue.Length > 0)
               {
                   SerialPanel.Port.Write(bvalue, 0, bvalue.Length);
                   this._txCount += bvalue.Length;
                   this.labTxCnt.Text = "" + this._txCount;
                }
            }
            catch (Exception ex)
            {
               MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        else
        {
            //just put out
            Console.WriteLine("Send string: " + str);
            try
            {
                SerialPanel.Port.Write(str);
                this._txCount += str.Length;
                this.labTxCnt.Text = "" + this._txCount;
            }
            catch (Exception ex)
            {
               MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //post msg to the button 
               SerialPanel.Exception_close();
            }
        }
   }
```

#### step4 处理显示部分

显示部分的内容和发送比较类似，这里先模仿发送先写单选框的处理部分。

差异部分为：如果16显示转字符串显示失败，就直接跳过，不提示用户了。单选框代码如下：

```c#
private void cbHexRecieve_CheckedChanged(object sender, EventArgs e)
{
    if (string.IsNullOrEmpty(rtbReceive.Text))
    {
        //nothing to do
        return;
    }
    //
    CheckBox cbbtn = sender as CheckBox;
    if (cbbtn.Checked)
    {
        //string to hex
        byte[] bvalue = System.Text.ASCIIEncoding.ASCII.GetBytes(rtbReceive.Text);
        string str = HexConvert.ToHexString(bvalue);
        rtbReceive.Text = str;
    }
    else
    {
        //hex to string
        try
        {
            byte[] bvalue = HexConvert.ToBytes(rtbReceive.Text);
            string str = System.Text.ASCIIEncoding.ASCII.GetString(bvalue);
            rtbReceive.Text = str;
        }
        catch (Exception ex)
        {
            Console.WriteLine("Ex for hex send " + ex.Message);
      
    }
}
```
接收的话通过事件进行，在PortOpen处理时加上事件绑定

```c#
private void PortOpened_Event(object sender, EventArgs e)
{
    btnSend.Enabled = true;
    //bind the event handle to data receive
    SerialPanel.Port.DataReceived += Port_DataReceived;
}
```
接收代码如下：

```c#
private void Port_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
{
    try
    {
        SerialPort port = sender as SerialPort;
        string rx = port.ReadExisting();
        this._rxCount += rx.Length;
        string prefix = "";
        //check hex
        if (cbHexRecieve.Checked)
        {
            byte[] bvalue = System.Text.ASCIIEncoding.ASCII.GetBytes(rx);
            rx= HexConvert.ToHexString(bvalue);
            //last char is space or "\r" "\n"
            if (!string.IsNullOrEmpty(rtbReceive.Text))
            {
                if(!rtbReceive.Text.EndsWith(" ") )
                {
                    prefix = " ";
                }
            }
        
        rx = prefix + rx;
        if (this.InvokeRequired)
        {
            this.Invoke(new Action(() =>
            {
                if (this.rtbReceive.Text.Length > 10240)
                {
                    try
                    {
                        LockWindowUpdate(rtbReceive.Handle);
                        this.rtbReceive.Text = this.rtbReceive.Text.Substring(this.rtbReceive.Text.Length - 10240);
                          this.rtbReceive.AppendText(rx);
                      }
                      finally
                      {
                          LockWindowUpdate(IntPtr.Zero);
                      }

                  }
                  else
                  {
                      this.rtbReceive.AppendText(rx);
                  }

                  this.labRxCnt.Text = "" + this._rxCount;
              }));
          }

      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
}
```
注意点：

​	 1、接收事件是发生在辅助线程上，如果操作控件需要进行invoke

​         2、对于textbox的内容更新，发生截断时会闪烁，此处直接调用了DLL进行重绘关闭（高级使用）

#### step5 时间戳

在接收到的内容前加上前缀即可，代码如下：

```c#
//Add time stamp
if (cbTimeStamp.Checked)
{
    //
    DateTime datetime = DateTime.Now;
    prefix = datetime.ToString("\r\n[yy-mm-dd HH:mm:ss fff]");
}
```
#### step6 定时发送

#### step7 日志

#### step8 字符支持

#### step9 完善

1、加上清除屏幕的按钮

```c#
private void btnClrRx_Click(object sender, EventArgs e)
{
    rtbReceive.Text = "";

private void btnClrTx_Click(object sender, EventArgs e)
{
    rtbSend.Text = "";
}
```
#### step10 其他可利用扩展


​     





 





