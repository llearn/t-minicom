﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;

namespace sluan.UI
{
    public partial class CSerialPanel : UserControl
    {
        public EventHandler PortOpened;
        public EventHandler PortClosed;
        private SerialPort _port = null;

        public SerialPort Port { get => _port; set => _port = value; }

        public CSerialPanel()
        {
            InitializeComponent();
        }

        private void CSerialPanel_Load(object sender, EventArgs e)
        {
            //load the port list
            btnFresh_Click(null, null);
            // baudrate,default 115200
            cmbBaud.SelectedIndex = 6;
        }

        private void btnFresh_Click(object sender, EventArgs e)
        {
            cmbPorts.Items.Clear();
            string[] portList = SerialPort.GetPortNames();
            if (portList.Length > 0)
            {
                cmbPorts.Items.AddRange(portList);
                cmbPorts.SelectedIndex = 0;
                btnOpen.Enabled = true;
            }
            else
            {
                btnOpen.Enabled = false;
            }
        }

        private void CtrLock(bool enable)
        {
            cmbBaud.Enabled = !enable;
            cmbPorts.Enabled = !enable;
            btnFresh.Enabled = !enable;
        }

        public void Exception_close()
        {
            btnOpen.Text = "Close";
            btnOpen_Click(btnOpen, null);
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            if (btn.Text == "Open")
            {
                //open the port
                string portname = this.cmbPorts.SelectedItem.ToString();
                Console.WriteLine("Port name:" + portname);

                try
                {
                    _port = new SerialPort(portname);
                    _port.BaudRate = Convert.ToInt32(cmbBaud.SelectedItem.ToString());
                    _port.Open();         
                    if (PortOpened != null)
                    {
                        PortOpened(sender, e);
                    }
                    btn.Text = "Close";
                    CtrLock(true);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    _port.Close();
                    _port = null;
                }

            }
            else
            {
                //close the port ,always ok
                try
                {
                    _port.Close();
                }
                finally
                {
                    btn.Text = "Open";
                    CtrLock(false);
                    _port = null;
                    if (PortClosed != null)
                    {
                        PortClosed(sender, e);
                    }
                }
            }
        }
    }
}
